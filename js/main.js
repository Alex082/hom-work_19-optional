// Создаём функцыю которая будет принемать 3 параметра

const willMeetDadline = (speed, baclog, dadline) => {
  // скорость команды

  let totalSpeed = speed.reduce((actual, correct) => actual + correct, 0);

  // количество страниц
  let totalPoint = baclog.reduce((actual, correct) => actual + correct, 0);

  // количество дней до заверншения, дедлайна
  let remainingDay = Math.ceil(totalPoint / totalSpeed);

  // текущая дата
  const today = new Date();

  // количество millisecondOneDay
  const millisecondOneDay = 1000 * 60 * 60 * 24;

  // остаток секунд до дедлайн
  const remainingSecond = dadline.getTime() - today.getTime();

  // количество рабочих дней
  const remainingWorkDay = Math.floor(
    (remainingSecond / millisecondOneDay / 7) * 5
  );

  //делаем проверку успеют или нет и выводим оповещение в консоль

  if (remainingDay <= remainingWorkDay) {
    console.log(
      `Усі завдання будуть успішно виконані за ${remainingDay} днів до настання дедлайну!`
    );
  } else {
    let aditionalHours = Math.ceil((remainingDay - remainingWorkDay) * 8);

    console.log(
      `Команді розробників доведеться витратити додатково ${aditionalHours} годин після дедлайну, щоб виконати всі завдання в беклозі!`
    );
  }
};
